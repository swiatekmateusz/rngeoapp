import React,{useState,useEffect,useRef} from 'react';

import { View,Text ,StyleSheet,Switch,Image} from 'react-native'


const GeoItem = ({coords: {timestamp,latitude,longitude,key},setGeoItem,switchRef}) => {
  const [isEnabled, setIsEnabled] = useState(false);

  useEffect(()=>{
    switchRef(previousState => [...previousState,setIsEnabled])
  },[])

  const toggleSwitch = () => {
    
    if(!isEnabled){
      const item = {
        latitude,
        longitude,
        key,
      }
      setGeoItem(previousState=>[...previousState,item])
    }else{
      setGeoItem(previousState=>previousState.filter(item=>item.key !== key))
    }
    setGeoItem(tab=>{
      console.log(tab)
      return tab
    })
    setIsEnabled(previousState => !previousState);
  }

  const date = new Date( parseInt(timestamp) );
  const year = date.getFullYear()
  const month =  ((date.getMonth()+1)>9 ? '' : '0') + (date.getMonth()+1)
  const day =  ((date.getDay()-1)>9 ? '' : '0') + (date.getDay()-1)

  const hour = ((date.getHours())>9 ? '' : '0') + date.getHours()
  const minutes = ((date.getMinutes())>9 ? '' : '0') + date.getMinutes()
  const dateString = `${year}-${month}-${day} ${hour}:${minutes}`

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image
        style={styles.image}
          source={require('../assets/earth.png')}
          resizeMode="center"
        />
      </View>
      <View style={styles.item}>
        <Text>Data: {dateString}</Text>
        <Text>Latitude: {latitude}</Text>
        <Text>Longitude: {longitude}</Text>
      </View>
      <View style={styles.switch}>
        <Switch
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  imgContainer:{
    paddingVertical: 10,
    height: 100,
    flex:2,
    justifyContent: "center",
    alignItems: "center",
  },
  container:{
    flexDirection: 'row',
    justifyContent: "space-between"
  },
  item:{
    paddingVertical: 10,
    paddingHorizontal: 16,
    flex:3,
    justifyContent:"center",
  },
  switch:{
    justifyContent:"center",
    alignItems: "center",
    flex:1,
  },
  image:{
    width:"100%",
    height: "100%",
  }
})

export default GeoItem;