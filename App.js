import React,{useEffect,useState} from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import StartScreen from './screens/StartScreen'
import ListScreen from './screens/ListScreen'
import MapScreen from './screens/MapScreen'
import {AppLoading} from 'expo'
import * as Font from "expo-font";

const Stack = createStackNavigator();

const fetchFonts =  ()=>{
  return Font.loadAsync({
    'nerkoone': require('./assets/NerkoOne-Regular.ttf'),
  });
}

export default function App() {
  const [fontLoaded,isFontLoaded] =useState(false)

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => isFontLoaded(true)} 
        onError={(err) => console.log(err)}
      />
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Start" component={StartScreen}  
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen name="Zapis" component={ListScreen}  />
        <Stack.Screen name="Map" component={MapScreen}  />
      </Stack.Navigator>
    </NavigationContainer>
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
