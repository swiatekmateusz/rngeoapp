import React,{useState,useEffect, useRef} from 'react';
import { View ,StyleSheet, TouchableOpacity,Switch,Text,AsyncStorage,FlatList } from 'react-native'

import GeoItem from '../components/GeoItem'

import * as Permissions from "expo-permissions";
import * as Location from "expo-location";

setPermissions = async () => {
  let { status } = await Permissions.askAsync(Permissions.LOCATION);
  if (status !== 'granted') {
      alert('Odmawiam przydzielenia uprawnień do czytania lokalizacji')
  }
}

const UsersScreen = (props) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const [isGettingPos, setIsGettingPos] = useState(false);
  const [geoList,setGeoList] = useState([])
  const [geoSelected, setGeoSelected] = useState([])
  const [switchRefs,setSwitchRef] = useState([])

  const toggleSwitch = () => {
    switchRefs.forEach(set=>{
      set(!isEnabled)
    })
    if(!isEnabled){
      const newGeoSelected = geoList.map(({latitude,longitude,key})=>{
        const item = {
          latitude,
          longitude,
          key,
        }
        return item
      })
      setGeoSelected(newGeoSelected)
    }else{
      setGeoSelected([])
    }
    console.log(geoSelected)
    setIsEnabled(previousState => !previousState);
  }

  const getData = async () => {
    let keys = await AsyncStorage.getAllKeys();
    //console.log("keys", keys)
    let stores = await AsyncStorage.multiGet(keys);
    //console.log("stores", stores)
    const geoList = []
    let maps = stores.map((result, i, store) => {
      let value = store[i][1];
      geoList.push({...JSON.parse(value),enable:false})
    });
    geoList.sort((a,b)=>{
      if(a.timestamp > b.timestamp) return -1
      else return 1
    })
    setGeoList(geoList)
  }

  useEffect(()=>{
    setPermissions()
    getData()
  },[])

  const getPosition = async () => {
    setIsGettingPos(true)
    let pos = await Location.getCurrentPositionAsync({})
    alert(JSON.stringify(pos, null, 4))
    const key = 'key' +  Math.round(Math.random()*10000)
    const newLocation = {
      key,
      timestamp: pos.timestamp,
      latitude: pos.coords.latitude,
      longitude: pos.coords.longitude,
    }
    console.log(newLocation)
    geoList.sort((a,b)=>{
      if(a.timestamp > b.timestamp) return -1
      else return 1
    })
    setGeoList([newLocation,...geoList])
    await AsyncStorage.setItem(key,JSON.stringify(newLocation));
    setIsGettingPos(false)
  }

  const deleteData = () =>{
      AsyncStorage.getAllKeys()
          .then(keys => AsyncStorage.multiRemove(keys))
          .then(() => setGeoList([]))
          .then(() => setGeoSelected([]))
          .then(() => setSwitchRef([]))
          .then(() => alert('Usunięto wszystkie dane'));
  }

  const handlePress=()=>{
    if(geoSelected.length){
      props.navigation.push("Map",geoSelected)
    }
    
  }

  return (
  <View style={{flex:1}}> 
    <View style={styles.buttons}>
      <TouchableOpacity onPress={getPosition} disable={isGettingPos}>
        <View>
          <Text>{!isGettingPos ? 'POBIERZ I ZAPISZ POZYCJĘ' : 'CZEKAJ...'}</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={deleteData}>
        <View>
          <Text>USUŃ WSZYSTKIE DANE</Text>
        </View>
      </TouchableOpacity>
    </View>
    <View style={styles.goToMpas}>
      <TouchableOpacity onPress={handlePress}><Text>PRZEJDŹ DO MAPY</Text></TouchableOpacity>
      <Switch
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
    </View>
    <FlatList
      data={geoList}
      renderItem={({ item }) => (
        <GeoItem key={item.key} coords={item} setGeoItem={setGeoSelected} switchRef={setSwitchRef}/>
      )}
      keyExtractor={item => item.key}
      contentContainerStyle={{
        paddingTop: 20,
        paddingBottom: 10,
      }}
    />
  </View>
  )
}

const styles = StyleSheet.create({
  buttons: {
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  goToMpas:{
    flexDirection: 'row',
    justifyContent: "space-between",
    padding: 15,
  }
});

export default UsersScreen