import React from 'react';
import MapView from 'react-native-maps';
import { View,StyleSheet,Text } from 'react-native'


const MapScreen = ({route:{params}}) =>{
  return (
    <MapView
      style={{ flex: 1 }}
      initialRegion={{
          latitude: params[0].latitude,
          longitude: params[0].longitude ,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
      }}
    >
      {params.map(item=><MapView.Marker
          coordinate={{
                latitude:item.latitude,
                longitude: item.longitude,
            }}
            key={item.key}
        />)}
    </MapView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


export default MapScreen