import React from 'react';

import { View ,StyleSheet,Text, TouchableOpacity } from 'react-native'


const LoginScreen = (props) =>{
  const handlePress=()=>{
    props.navigation.navigate("Zapis")
  }
 
  return (
  <View> 
    <View style={styles.header}>
      <Text style={styles.mainText}>GeoMap App</Text>
      <Text style={styles.subText}>find and save your position</Text>
    </View>
    <View style={{...styles.header,...styles.buttonContainer}}>
      <TouchableOpacity onPress={handlePress}>
        <Text style={styles.button}>
          Start
        </Text>
      </TouchableOpacity>
    </View>
  </View>
  )
}

const styles = StyleSheet.create({
  header: {
    height:'40%',
    justifyContent:"center",
    alignItems: "center",
    
  },
  buttonContainer:{
    height: "60%",
    backgroundColor: '#dfdfdf',
  },
  button:{
    fontSize: 20,
  },
  mainText:{
    fontSize: 42,
    fontFamily: "nerkoone",
  },
  subText:{
    fontSize: 20,
    fontFamily: "nerkoone",
  }
});

export default LoginScreen